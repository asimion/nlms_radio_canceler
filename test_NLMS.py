import padasip as pa
import matplotlib.pylab as plt
from scipy.io import wavfile
import numpy as np
import math as m
import time
import threading

lock = threading.Lock()
NUM_TH = 5

MU_ARR = [0.001 , 0.025, 0.050, 0.075, 0.100, 0.125, 0.150, 0.175, 0.200, 0.225, 0.250]
ORDER_ARR = [16, 32, 64, 128, 256, 512]
dp = len(MU_ARR) * [[None]]
xv = len(MU_ARR) * [[None]]
EPS = 0.024
aprox_zero = 1e-5


def print_matrix(mat):
    
    for i in mat:
        for j in i:
            print(j)
        print()

def convert_int16_to_float64(x):
    
    return x / 2**15
        
def compute_abs_max(my_list):
    
    abs_vals = [abs(i) for i in my_list]
    return max(abs_vals)

def scale_range(s):
    ''' aprox conversion int to float '''
    maxim = compute_abs_max(s)
    return [i/maxim for i in s]

def cpy_right(x):
    
    for i in range(len(x)-1, 0, -1):
        x[i] = x[i-1]
    return x

def radio_cancellation(x, d, N, a_eps, u):
    print("hhhhh")
    x_len = len(x)
    d_len = len(d)
    
    if(x_len < d_len):
        dif = d_len - x_len
        padding = [a_eps] * dif
        x = np.concatenate((x, padding))
    elif x_len > d_len:
        dif = x_len - d_len
        padding = [a_eps] * dif
        d = np.concatenate((d, padding))

    f = pa.filters.FilterNLMS(n = N, mu = u, eps = a_eps, w = "zeros")
    x_f = [N * [0]]
    idx = 0
    out = []
    err = []
    for x_i in x:
	    x_f[0] = np.concatenate(([x_i], x_f[0][1:N]))
	    di = np.array([d[idx]])
	    y, e, w = f.run(di, x_f)
	    out.append(y)
	    err.append(e)
	    x_f[0] = cpy_right(x_f[0])
	    idx = idx + 1

    err = 1/compute_abs_max(err) * err
    out = 1/compute_abs_max(out) * out
    err=np.array(err, dtype="float")
    out=np.array(out, dtype="float") 	    
    
    return out, err

def compute_power(signal):
    
    N = len(signal)
    signal = np.power(signal, 2)
    return sum(signal) * 1/N

def delta_power(s, s_hat):
    
    s = convert_int16_to_float64(s)
    return abs( compute_power( s ) - compute_power( s_hat ) )

def delta_power_windowing(s, s_hat):
    
    delta_pow = []
    s_len = len(s)
    s_h_len = len(s_hat)
    #s = convert_int16_to_float64(s)
    s_h = []
    s_hat = np.transpose(s_hat)
    if s_h_len % s_len != 0:
        zeros_padding = ( (m.ceil(s_h_len / s_len) * s_len ) - s_h_len) * [0]
        print(s_hat.shape)
        s_h = np.concatenate((s_hat[0], zeros_padding))
        s_h_len = len(s_h)
    else:
        print(s_hat.shape)
        s_h = s_hat[0]
        s_h_len = len(s_h)
    i = s_len
    start = 0
    print(f's_h_len : {s_h_len}')
    while i <= s_h_len:
        window_s_hat = []
        print(f'{start} # {i}')
        for j in range(start, i, 1):
            window_s_hat.append(s_h[j])
        delta_pow.append( delta_power(s, window_s_hat ) )
        start = i
        i = i + s_len
    return min(delta_pow)

def max_corr(s0, s1):
    
    cross_corr = np.correlate(s0, s1, "full")
    max_val = max(cross_corr)
    idx_max_val = [i for i in cross_corr if i == max_val]
    return max_val, idx_max_val[0]

def xcorr_windowing(s, s_hat):
    
    x_corr = []
    s_len = len(s)
    s_h_len = len(s_hat)
    s = convert_int16_to_float64(s)
    s_h = []
    s_hat = np.transpose(s_hat)
    if s_h_len % s_len != 0:
        zeros_padding = ( (m.ceil(s_h_len / s_len) * s_len ) - s_h_len) * [0]
        s_h = np.concatenate((s_hat[0], zeros_padding))
    else:
        s_h = s_hat[0]
        s_h_len = len(s_h)
    i = s_len
    start = 0
    while i <= s_h_len:
        window_s_hat = []
        for j in  range(start, i, 1):
            window_s_hat.append(s_h[j])
        max_val, _ = max_corr(s, window_s_hat)
        x_corr.append(max_val)
        start = i
        i = i + s_len
    return max(x_corr)

def grid_search_hyperparams(fnc_test1, fnc_test2, cmd, x, d, itPow_start, itPow_stop, mu_start, mu_stop, tol):
    
    y, e = 0, 0
    #min_delta = 1
    filter_order = pow(2, itPow_start)
    adapt_step = mu_start 
    mat_delta_pow = []
    mat_xcorr_val = []
    #n = N_start
    for u in MU_ARR:
        #print(f' mu {u} :')
        row_delta_pow = []
        row_xcorr_val = []
        for it in range(itPow_start, itPow_stop):
            n = pow(2, it)
            print(f'mu : {u} -- Order : {n} ')
            y, e = radio_cancellation(x, d, n, aprox_zero, u)
            delta = fnc_test1(cmd, e)[0]
            xcorr_val = fnc_test2(cmd, e)
            row_delta_pow.append(delta)
            row_xcorr_val.append(xcorr_val)
            print(delta)
            print( xcorr_val)
            '''
            if min_delta > delta:
                min_delta = delta
                filter_order = n
                adapt_step = u
           '''
        mat_delta_pow.append(row_delta_pow)
        mat_xcorr_val.append(row_xcorr_val)
    return mat_delta_pow, mat_xcorr_val, filter_order, adapt_step      


def th_grid_search(idx_th, fnc_test1, fnc_test2, cmd, x, d, rows_mat):
    
    global dp
    global xv
    y, e = 0, 0
    start = 0
    end = 0
    dim_part = rows_mat // NUM_TH
    r = rows_mat % NUM_TH
    if r == 0:
        start = idx_th
        end = start + dim_part
    elif idx_th < r:
        start = idx_th *(dim_part + 1)
        end = start + dim_part + 1
    else:
        start = idx_th * dim_part + r
        end = start + dim_part
    print(f"th {idx_th}")
    for i in range(start, end):
        row_delta_pow = []
        row_xcorr_val = []
        for n in ORDER_ARR:
            print(f'mu : {MU_ARR[i]} -- Order : {n} ')
            
            lock.acquire()
            y, e = radio_cancellation(x, d, n, aprox_zero, MU_ARR[i])
            lock.release()
            
            lock.acquire()
            delta = fnc_test1(cmd, e)[0]
            xcorr_val = fnc_test2(cmd, e)
            lock.release()
            
            print(delta)
            print( xcorr_val)
            row_delta_pow.append(delta)
            row_xcorr_val.append(xcorr_val)
        print(f"thread {idx_th} write")
        with lock:
            dp[i] = row_delta_pow
            xv[i] = row_xcorr_val
            
            
'''----- main ------'''

'''#### test database sequentially #####

cmd_files = "cmd.txt"
x_files = "x_signals.txt"
d_files = "radio.txt"
cmd_signals = []
x_signals = []
d_signals = []

with open(cmd_files, "r") as filehandler:
    filecontent = filehandler.readlines()
    for line in filecontent:
        cmd_signals.append(line.rstrip())

with open(x_files, "r") as filehandler:
    filecontent = filehandler.readlines()
    for line in filecontent:
        x_signals.append(line.rstrip())

with open(d_files, "r") as filehandler:
     filecontent = filehandler.readlines()
     for line in filecontent:
         d_signals.append(line.rstrip())

num_of_files = len(cmd_signals)
opt_pow = dict()
for i in  range(num_of_files):
    print(f'{i} processing')
    fs_x, data_x = wavfile.read('/home/asimion/Licenta_Documentatie/nlms_radio_canceler/v_r_1/' + x_signals[i])#radio+voice
    fs_d, data_d = wavfile.read('/home/asimion/radio_1/' + d_signals[i])#radio
    fs_cmd, cmd = wavfile.read('/home/asimion/cmd_wav/' + cmd_signals[i])#vocal cmd

    data_x = data_x / 2#scaling x input
    print(x_signals[i] + " " + d_signals[i] + " " + cmd_signals[i])
    delta, N, u = grid_search_hyperparams(delta_power, cmd, data_x, data_d, 64, 0.0326)
    
    touple_key = (x_signals[i], cmd_signals[i])
    val_arr = [u, N]
    opt_pow[touple_key] = val_arr
    
    print(f'delta power {delta}')
    print(f'filter order {N}')
    print(f'mu {u}')
    
print(opt_pow)
'''


#### find hyperparametes using using parallel computing ####
fs_x, data_x = wavfile.read('guerrilla_9_35^1564389290.wav')#radio+voice
fs_d, data_d = wavfile.read('guerrilla_9_35.wav')#radio
fs_cmd, cmd = wavfile.read('1564389290.wav')#vocal cmd
data_x = data_x / 2#scaling x input

t0 = time.time()
th = NUM_TH * [None]
for i in  range(NUM_TH):
    print(i)
    th[i] = threading.Thread(target=th_grid_search, args=(i, delta_power, xcorr_windowing, cmd, data_x, data_d, len(MU_ARR),  ))
    print(th[i])
    th[i].start()

for i in range(NUM_TH):
    print(i)
    th[i].join()
    
t1 = time.time()
t_f = (t1 - t0) // 60 
dp = np.matrix(dp)
xv = np.matrix(xv)
#xv = xv * 1 / xv.max()
print(f"parallel time {t_f}min")
cmd = convert_int16_to_float64(cmd)
acorr_max = np.correlate(cmd, cmd)[0]
print(acorr_max)
print("Delta Power")
print_matrix(dp)
print("XCORR")
print_matrix(xv)
xv = xv * 1 / acorr_max
print("XCORR_norm")
print_matrix(xv)
'''#### find hyperparameters sequentially for a pair ####  ~ 23mins

fs_x, data_x = wavfile.read('guerrilla_9_35^1564389290.wav')#radio+voice
fs_d, data_d = wavfile.read('guerrilla_9_35.wav')#radio
fs_cmd, cmd = wavfile.read('1564389290.wav')#vocal cmd
data_x = data_x / 2#scaling x input
data_d = convert_int16_to_float64(data_d)
t0 = time.time()
dp, xv, _, _= grid_search_hyperparams(delta_power, xcorr_windowing, cmd, data_x, data_d, 4, 10, 0.001, 0.25, EPS)
t1 = time.time()
t_f = (t1 - t0) // 60
print(f'timp seq exec : {t_f}min')
cmd = convert_int16_to_float64(cmd)
acorr_max = np.correlate(cmd, cmd)[0]
print(acorr_max)
dp = np.matrix(dp)
xv = np.matrix(xv)
print("Delta Power")
print_matrix(dp)
print("XCORR")
print_matrix(xv)
xv = xv * 1 / acorr_max
print("XCORR_norm")
print_matrix(xv)

'''
##################################################################

'''
#### first four best values for NLMS for input digiFm_11_07^1563877711.wav #####

delta power 0.0002611042465086814
filter order 64
mu 0.0635


delta power 8.78652500668417e-06
filter order 64
mu 0.0328

delta power 6.987530875987233e-06
filter order 64
mu 0.0327

delta power 5.179574223598479e-06
filter order 64
mu 0.0326

'''

''' 
#### nlms filter test hyperparams #####


y, e = radio_cancellation(data_x, data_d, 64, 0, 0.0326)

wavfile.write("e.wav", rate=16000, data=e)
wavfile.write("y.wav", rate=16000, data=y)
'''
